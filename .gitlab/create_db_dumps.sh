#!/bin/bash -eu
set -o pipefail

CI=${CI:-}
PORT_NUM=23232
API_URL=http://localhost:$PORT_NUM
CONTAINER_NAME=db-dump-generator
API_READY_LOG=logs/api_is_ready.log
mapfile -t DATASETS < <(find testdata/analyses -mindepth 1 -maxdepth 1 \( -type d -o -type l \) -exec basename {} \;)

declare -A SKIP_DATASETS
for ds_name in "${DATASETS[@]}"; do
    SKIP_DATASETS["${ds_name}"]=
done
# custom breaks and doesn't seem to be used anywhere, so skip until we verify unused and delete it
SKIP_DATASETS[custom]=1
# only old frontend uses e2e and it expects freshly imported data, so archived webdumps will cause
# them to fail starting 7 days after they were created.
SKIP_DATASETS[e2e]=1

log() {
    echo "$(date -Iseconds) - $*"
}

jq() {
    command jq -er "$@"
}

# We could just use the scripts directly, but the healthcheck functionality is not available
# programmtically in other scripts at the moment. So we also use curl for the reset/dump commands
# for the sake of consistency.
# Curling inside the container is simpler than arguing with CI docker-in-docker.
curl() {
    docker exec $CONTAINER_NAME curl -Ss "$@"
    echo
}

api_is_ready() {
    if (($# != 2)); then
        log "ERROR: api_is_ready - expected 2 arguments: current_attempt, max_attempts"
        return 1
    fi
    local curr=$1
    local max=$2
    curl "${API_URL}/healthcheck" >>$API_READY_LOG
    if [[ $(tail -1 $API_READY_LOG | jq .status) != 200 ]]; then
        if ((curr < max)); then
            tail -1 $API_READY_LOG | jq '{ts: .ts, status: .message.status}'
        else
            tail -1 $API_READY_LOG | jq .
            docker logs "${CONTAINER_NAME}" &>logs/docker_output.log
            log "Failed to start backend"
        fi
        return 1
    fi
}

# create initial dirs with open write permission
{ umask 000 && mkdir -p logs dumps; }

# BACKEND_IMAGE provided from .gitlab-ci.yml
docker run \
    --name ${CONTAINER_NAME} \
    -e "CI=${CI}" \
    -e ANNOTATION_SERVICE_URL=placeholder \
    -e ATTACHMENT_STORAGE=/tmp \
    -e DB_URL=postgresql:///postgres \
    -e ENABLE_CNV=true \
    -e ENABLE_PYDANTIC=true \
    -e OFFLINE_MODE=false \
    -d \
    -p ${PORT_NUM}:${PORT_NUM} \
    -v /pg-data \
    -v "${PWD}/logs:/logs" \
    -v "${PWD}:/ella/ella-testdata" \
    "${BACKEND_IMAGE}" \
    supervisord -c /ella/ops/dev/supervisor.cfg

docker inspect $CONTAINER_NAME >>logs/container_config.log

MAX_RETRIES=10
ATTEMPT_NUM=0
SLEEP_TIME=15
until api_is_ready $ATTEMPT_NUM $MAX_RETRIES; do
    ATTEMPT_NUM=$((ATTEMPT_NUM + 1))
    if ((ATTEMPT_NUM > MAX_RETRIES)); then
        log "Failed to start backend"
        docker logs ${CONTAINER_NAME} &>logs/docker_output.log
        exit 1
    fi
    log "Waiting for backend to start (${ATTEMPT_NUM}/${MAX_RETRIES})"
    sleep $SLEEP_TIME
done
log "Backend started"

for dataset in "${DATASETS[@]}"; do
    if [[ -n ${SKIP_DATASETS[$dataset]} ]]; then
        log "Skipping dataset $dataset"
        continue
    fi
    log "Creating dump for ${dataset}"
    curl -d "testset=${dataset}" ${API_URL}/database/reset
    curl -d "testset=${dataset}" ${API_URL}/database/dump
done

docker rm -vf ${CONTAINER_NAME}
