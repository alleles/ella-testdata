#!/bin/bash -e
set -o pipefail

# Expected CI variables:
#    - $AWSCLI_IMAGE
#         - transparent to script, it's where the script is run inside
#    - $AWS_CONFIG_FILE
#         - provided as group level CI variable

S3_BUCKET=ella/testdata
LOCAL_DIR=${PWD}/dumps

aws --endpoint https://fra1.digitaloceanspaces.com s3 sync --no-progress --acl=public-read "${LOCAL_DIR}" "s3://${S3_BUCKET}"
