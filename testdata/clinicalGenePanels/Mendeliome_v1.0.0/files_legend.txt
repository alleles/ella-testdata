# Gene panel: Mendeliome-v1.0.0 -- File legend
- Mendeliome_v1.0.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- Mendeliome_v1.0.0_regions.bed:  exonic regions [longest frame fallback strategy]
- Mendeliome_v1.0.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- Mendeliome_v1.0.0.json:  normalized panel file
